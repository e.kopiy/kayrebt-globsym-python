#!/usr/bin/env python
# coding: utf8


import subprocess
import sqlite3


def get_formatted_string(inp_str):
    sym_num = 0
    result = ''
    words_cnt = 0

    while sym_num < len(inp_str):
        if inp_str[sym_num] == ' ':
            if words_cnt != 1:
                result = result + inp_str[:sym_num] + '|'
            inp_str = inp_str[sym_num:].strip()
            sym_num = 0
            words_cnt += 1
        if words_cnt == 4:
            break
        sym_num += 1

    result = result[: len(result) - 1]

    if result.find('/'):
        folder = '/' + result[result.rfind('|') + 1:result.rfind('/')]
        result = result[:result.rfind('|')] + '|' + folder + '|' + result[result.rfind('/') + 1:]
    else:
        result = result[:result.rfind('|')] + '||' + result[result.rfind('/') + 1:]

    tmp = result[:result.find('|')]
    result = result[result.find('|') + 1:]
    result = tmp + result[result.find('|'):] + '|' + result[:result.find('|')]
    return result


def get_func_prototypes():
    ctags_pipeline = "ctags -x --c-kinds=f --recurse=yes --exclude=*.h --exclude=/home/ekopiy/pie_light/test --exclude=CMakeFiles --exclude=*.txt --exclude=*.bash --exclude=*.cmake --exclude=*.ggo --exclude=Doxyfile --exclude=*.py > ctags.out"
    subprocess.check_call(['bash', '-c', ctags_pipeline])
    raw_prototypes = open(r'ctags.out')
    raw_proto_list = []

    for func in raw_prototypes.readlines():
        raw_proto_list.append(get_formatted_string(func))

    return raw_proto_list


def fill_db(inp_list):
    print inp_list
    conn = sqlite3.connect('url.sqlite')
    c = conn.cursor()
    c.execute('DELETE FROM global_symbols')
    for func_proto in inp_list:
        field_list = func_proto.split('|')
        query = 'INSERT INTO global_symbols (symbol, dir, file, line) VALUES ('
        query += "'" + field_list[0] + "'" + ', ' + "'" + field_list[1] + "'" + ', ' + "'" + field_list[2]
        query += "'" + ', ' + "'" + field_list[3] + "'" + ')'
        c.execute(query)
    conn.commit()
    conn.close()

def main():
    proto_list = get_func_prototypes()
    fill_db(proto_list)

if __name__ == "__main__":
    main()

